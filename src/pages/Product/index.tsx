import React from "react";
import HeaderFiknit from "../../components/HeaderFiknit";
import FooterFiknit from "../../components/FooterFiknit";
import ProductDetails from "../../components/ProductDetails";

const Product: React.FC = () => {
  return (
    <div className="App">
      <HeaderFiknit />
      <ProductDetails/>
      <FooterFiknit />
    </div>
  );
};
export default Product;
