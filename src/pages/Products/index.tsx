import React from "react";
import HeaderFiknit from "../../components/HeaderFiknit";
import ProductList from "../../components/ProductList";
import FooterFiknit from "../../components/FooterFiknit";

const Products: React.FC = () => {
  return (
    <div className="App">
      <HeaderFiknit />
      <ProductList />
      <FooterFiknit />
    </div>
  );
};
export default Products;
