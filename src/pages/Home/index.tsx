import React from "react";
import HeaderFiknit from "../../components/HeaderFiknit";
import FooterFiknit from "../../components/FooterFiknit";
import PhotoBan from "../../components/PhotoBan";
import News from "../../components/News";
import MarieInfos from "../../components/MarieInfos";

const Home: React.FC = () => {
  return (
    <div className="App">
      <HeaderFiknit />
      <PhotoBan />
      <News />
      <MarieInfos />
      <FooterFiknit />
    </div>
  );
};
export default Home;
