import React from "react";
import HeaderFiknit from "../../components/HeaderFiknit";
import FooterFiknit from "../../components/FooterFiknit";
const About: React.FC = () => {
  return (
    <div className="App">
      <HeaderFiknit />
      <p>ABOUT</p>
      <FooterFiknit />
    </div>
  );
};
export default About;
