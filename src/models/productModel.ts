export interface Product {
    id: number;
    name: string;
    picture: string;
    price: string;
    quantityStock: number;
}