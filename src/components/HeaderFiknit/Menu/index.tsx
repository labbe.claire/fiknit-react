import React from "react";
import "../Header.css";

const Menu: React.FC = () => {
  return (
    <div>
      <ul>
        <li>
          <a href="/">Accueil</a>
        </li>
        <li>
          <a href="/products">Produits</a>
        </li>
        <li>
          <a href="/about">A propos</a>
        </li>
        <li>
          <a href="/contact">Contact</a>
        </li>
        <li>
          <a href="/cart">Panier</a>
        </li>
      </ul>
    </div>
  );
};

export default Menu;
