import React from "react";
import Logo from "./Logo";
import Menu from "./Menu";
import './Header.css';


const HeaderFiknit : React.FC = () =>
{
    return (
    <div className="header1">
        <Logo/>
        <Menu/>
        </div>
    )
};

export default HeaderFiknit;