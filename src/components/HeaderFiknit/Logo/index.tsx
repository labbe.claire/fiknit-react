import React from "react";

const logoHeader = "https://i.ibb.co/JvbqycV/logo-1.png";

const Logo : React.FC = () =>
{
    return (
        <div>
            <img src={logoHeader} alt="logo Fiknit" className="logo"/>
        </div>
    )
};

export default Logo;