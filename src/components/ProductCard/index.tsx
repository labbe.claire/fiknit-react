import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import { Product } from "../../models/productModel";
import React from "react";

function ProductCard({product}: {product : Product} ) {
  return (
    <Card style={{ width: "auto", margin: "5rem" }}>
      <a href={`/products/${product.id}`}>
        <Card.Img variant="top" src={product.picture} />
      </a>
      <Card.Body>
        <Card.Title>{product.name}</Card.Title>
        <Card.Text>{product.price},00 €</Card.Text>
        <Button style={{ background: "#1bb55e" }}>Ajouter au panier</Button>
      </Card.Body>
    </Card>
  );
}

export default ProductCard;
