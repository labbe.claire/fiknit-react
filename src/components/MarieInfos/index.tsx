import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import "./MarieInfos.css";

const photoInfos = "https://i.ibb.co/Bf23NKK/wool-3491905-1920.jpg";

const MarieInfos: React.FC = () => {
  return (
    <Container
      style={{
        display: "flex",
        padding: "20px",
        maxWidth: "fit-content",
        maxHeight: "fit-content",
        justifyContent: "center",
        alignContent: "center",
        margin: "8rem",
      }}
    >
      <Row className="col-12">
        <Col className="col-8">
          <h2 className="text-start pb-4">Actualités de Marie</h2>
          <p className="infoText">
            Lorem ipsum dolor sit amet. Non officiis rerum in dolor eaque ut
            soluta aliquam qui numquam quia? Aut alias dolorum et consequatur
            dolorem rem veritatis omnis vel rerum sint.
          </p>
          <p className="infoText">
            Prochains salons : Lorem ipsum dolor sit amet. Non officiis rerum in
            dolor eaque ut soluta aliquam qui numquam quia? Aut alias dolorum et
            consequatur dolorem rem veritatis omnis vel rerum sint.
          </p>
          <p className="infoText">
            Prochaines fêtes : Lorem ipsum dolor sit amet. Non officiis rerum in
            dolor eaque ut soluta aliquam qui numquam quia? Aut alias dolorum et
            consequatur dolorem rem veritatis omnis vel rerum sint.
          </p>
        </Col>
        <Col className="col col-4">
          <img
            src={photoInfos}
            alt="photo3PairesDeChaussons"
            style={{ width: "360px", height: "480px", objectFit: "cover" }}
          />
        </Col>
      </Row>
    </Container>
  );
};

export default MarieInfos;
