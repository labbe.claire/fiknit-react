import React, { useEffect, useState } from "react";
import UserService from "../../services/userService";
import { User } from "../../models/userModel";

const UserComponent: React.FC = () => {
  const [users, setUsers] = useState<User[]>([]);

  useEffect(() => {
    UserService.getAllUsers()
      .then((data) => {
          setUsers(data); 
          console.log(data, "!");
      })
      .catch((error) => {
        console.error("Erreur de requête :", error);
      });
  }, []);
    
    return (
        <div>
            {users.map(user => (
                <div key={user.id}>{user.name}</div>
            ))}
        </div>
    );
};

export default UserComponent;
