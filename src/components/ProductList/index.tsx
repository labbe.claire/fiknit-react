import React, { useEffect, useState } from "react";
import ProductService from "../../services/productService";
import { Product } from "../../models/productModel";
import ProductCard from "../ProductCard";

const ProductList: React.FC = () => {
  const [products, setProducts] = useState<Product[]>([]);

  useEffect(() => {
    ProductService.getAllProducts()
      .then((data) => {
        setProducts(data);
        console.log(data, "!");
      })
      .catch((error) => {
        console.error("Erreur de requête :", error);
      });
  }, []);

  return (
      <div className="grid grid-cols-3 gap-2 justify-between mx-24">
        {products.map((product) => (
          <div key={product.id}>
            <ProductCard product={product} />
          </div>
        ))}
      </div>
  );
};

export default ProductList;
