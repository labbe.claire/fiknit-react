import React from "react";
import './PhotoBan.css';

const photoban = "https://i.ibb.co/p4fpcrQ/photoban.jpg";

const PhotoBan : React.FC = () =>
{
    return (
    <div>
        <img src={photoban} alt="photo3PairesDeChaussons" className="ban"/>
        </div>
    )
};

export default PhotoBan;