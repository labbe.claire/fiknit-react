import React, { useEffect, useState } from "react";
import ProductService from "../../services/productService";
import { Product } from "../../models/productModel";
import { Col, Container, Row } from "react-bootstrap";
import ImgNews from "./ImgNews";

const News: React.FC = () => {
  const [products, setProducts] = useState<Product[]>([]);

  useEffect(() => {
    ProductService.getAllProducts()
      .then((data) => {
        setProducts(data);
        console.log(data, "!");
      })
      .catch((error) => {
        console.error("Erreur de requête :", error);
      });
  }, []);

  return (
    <Container
          style={{
          display:"flex",
        padding: "20px",
        maxWidth: "fit-content",
        maxHeight: "fit-content",
        backgroundColor: "#f5f5f5",
        justifyContent: "center",
        alignContent: "center",
        margin: "8rem",
      }}
    >
          <Row>
              <h2 className="text-start">Nouveautés</h2>
        <div className="d-flex content-center">
          {products.map((product) => (
            <div key={product.id}>
              <Col>
                <ImgNews product={product} />
              </Col>
            </div>
          ))}
        </div>
      </Row>
    </Container>
  );
};

export default News;
