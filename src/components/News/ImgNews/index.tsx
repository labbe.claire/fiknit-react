import React from "react";
import "../News.css";
import Image from "react-bootstrap/Image";
import { Product } from "../../../models/productModel";

function ImgNews({ product }: { product: Product }) {
  return <Image className="newImg" src={product.picture} rounded />;
}

export default ImgNews;
