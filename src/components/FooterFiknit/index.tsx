import React from "react";
import LogoFoot from "./LogoFoot";
import Reseaux from "./Reseaux";
import MenuFoot from "./MenuFoot";
import "./Footer.css";

const FooterFiknit : React.FC = () =>
{
    return (
      <div className="footer1">
        <LogoFoot />
        <MenuFoot />
        <Reseaux />
      </div>
    );
};

export default FooterFiknit;