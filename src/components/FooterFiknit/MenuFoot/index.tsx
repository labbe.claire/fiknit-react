import React from "react";
import "../Footer.css";

const MenuFoot : React.FC = () =>
{
    return (
        <div>
            <ul>
                <li><a href="/">Accueil</a></li>
                <li><a href="/products">Produits</a></li>
                <li><a href="/about">A propos</a></li>
                <li><a href="/contact">Contact</a></li>
            </ul>
        </div>
    )
};

export default MenuFoot;

//Bootstrap installé