import React from "react";
import { BsFacebook, BsInstagram } from "react-icons/bs";

const Reseaux: React.FC = () => {
  return (
    <div className="footer1">
      <ul>
        <li><a href="lien"><BsFacebook /></a></li>
        <li><a href="lien"><BsInstagram /></a></li>
      </ul>
    </div>
  );
};

export default Reseaux;
