import React from "react";

const logoFoot = "https://i.ibb.co/jwTWvr2/logo-2-trans.png";

const LogoFoot: React.FC = () => {
  return (
    <div>
      <img src={logoFoot} alt="logo Fiknit blanc" className="logoFoot"/>
    </div>
  );
};

export default LogoFoot;
