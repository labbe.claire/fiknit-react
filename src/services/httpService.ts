import { AxiosResponse, AxiosError } from "axios";
import instance from "../configuration/axiosConfig";


export const HttpService = {
    get: async <T>(url: string): Promise<T> => {
        try {
            const response: AxiosResponse<T> = await instance.get<T>(url);
            return response.data;
        } catch (error : any) {
            throw (error as AxiosError).response?.data || error.message;
        }
    },
    post: async <T>(url: string, data: any): Promise<T> => {
        try {
            const response: AxiosResponse<T> = await instance.post<T>(url, data);
            return response.data;
        } catch (error : any) {
            throw (error as AxiosError).response?.data || error.message;
        }
    },
    put: async<T>(url: string, data: any): Promise<T> => {
        try {
            const response: AxiosResponse<T> = await instance.put<T>(url, data);
            return response.data;
        } catch (error: any) {
            throw (error as AxiosError).response?.data || error.message;
        }
    },
    delete: async<T>(url: string) => {
        try {
            await instance.delete<T>(url);
        } catch (error: any) {
            throw (error as AxiosError).message;
        }
    },
};

