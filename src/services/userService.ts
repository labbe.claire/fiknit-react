import { HttpService } from './httpService';
import { User } from '../models/userModel';

const UserService = {
    getAllUsers: async () => {
        return await HttpService.get<User[]>('/users');
    },
    getUserById: async (id: number) => {
        return await HttpService.get<User>(`/users/${id}`);
    },
    createUser: async (userData: User) => {
        return await HttpService.post<User>('/users', userData);
    },
    updateUser: async (userData: User) => {
        return await HttpService.put<User>('/users', userData)
    },
    deleteUser: async (id: number) => {
        return await HttpService.delete<User>(`/users/${id}`);
    }

};
export default UserService;