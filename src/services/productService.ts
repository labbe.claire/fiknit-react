import { Product } from '../models/productModel';
import { HttpService } from './httpService';


const ProductService = {
    getAllProducts: async () => {
        return await HttpService.get<Product[]>('/product');
    },
    getProductById: async (id: number) => {
        return await HttpService.get<Product>(`/product/${id}`);
    },
    createProduct: async (ProductData: Product) => {
        return await HttpService.post<Product>('/product', ProductData);
    },
    updateProduct: async (ProductData: Product) => {
        return await HttpService.put<Product>('/product', ProductData)
    },
    deleteProduct: async (id: number) => {
        return await HttpService.delete<Product>(`/product/${id}`);
    }

};
export default ProductService;